from django.contrib import admin

# Register your models here.

from .models import *


class ChoiceInline(admin.TabularInline):
    '''
    Hier legen Sie fest, wie viele leere Formularfelder für neue Choices
    bei der Station angezeigt werden sollen. Der fk_name muss angegeben
    werden, da Choice zwei verschiedene FKs auf Station hat.
    '''
    model = Choice
    fk_name = 'station'
    extra = 1

class StationAdmin(admin.ModelAdmin):
    '''
    Hier legen Sie fest, welche Elemente in der Admin-Oberfläche in der
    Übersicht angezeigt werden und dass man beim Bearbeiten einer Station
    direkt die Choices der Station bearbeiten kann (inline).
    '''
    inlines = [ChoiceInline, ]
    list_display = ['name', 'text', "get_choices"]
    def get_choices(self, obj):
        return " / ".join([str(o) for o in obj.choice_set.all()])
    get_choices.short_description = "choices"

admin.site.register(Station, StationAdmin)



class ChoiceInline(admin.TabularInline):
    '''
    Hier legen Sie fest, wie viele leere Formularfelder für neue Choices
    bei der Station angezeigt werden sollen. Der fk_name muss angegeben
    werden, da Choice zwei verschiedene FKs auf Station hat.
    '''
    model = Choice
    fk_name = 'station'
    extra = 1

class StationAdmin(admin.ModelAdmin):
    '''
    Hier legen Sie fest, welche Elemente in der Admin-Oberfläche in der
    Übersicht angezeigt werden und dass man beim Bearbeiten einer Station
    direkt die Choices der Station bearbeiten kann (inline).
    '''
    inlines = [ChoiceInline, ]
    list_display = ['name', 'text', "get_choices"]
    def get_choices(self, obj):
        return " / ".join([str(o) for o in obj.choice_set.all()])
    get_choices.short_description = "choices"

#admin.site.register(Station, StationAdmin)
