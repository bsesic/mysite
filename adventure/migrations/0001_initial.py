# Generated by Django 3.1 on 2021-05-05 10:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('title', models.CharField(max_length=200)),
                ('text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=200)),
                ('next_station', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='incoming_set', to='adventure.station')),
                ('station', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='adventure.station')),
            ],
        ),
    ]
