from django.db import models

# Create your models here.

class Station(models.Model):
    '''
    Eine Station im Textadventure. Der Name ist eine sprechende ID,
    muss also eindeutig (unique) sein.
    '''
    name = models.CharField(max_length=50, unique=True)
    title = models.CharField(max_length=200)
    text = models.TextField()
    image = models.ImageField(upload_to='', max_length=200, null=True)
    def __str__(self):
        return self.name

class Choice(models.Model):
    '''
    Eine Auswahloption. Neben dem Text zur Beschreibung enthält Sie auch den
    Verweis auf die nächste Station.
    '''
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    next_station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="incoming_set")

    def __str__(self):
        return f"{self.text}: {self.next_station.name}"



class Player(models.Model):
    name = models.CharField(max_length=200, null = False)
    experience = models.IntegerField(default=0)
    health = models.IntegerField(default=100)
    def __str__(self):
        return self.name

class Item(models.Model):
    item = models.CharField(max_length=200)
    image = models.ImageField(upload_to='item', max_length=200, null=True)
    def __str__(self):
        return self.item

class Inventory_entry(models.Model):
    # many-to-many relation
    player = models.ForeignKey(to=Player, on_delete=models.CASCADE)
    item = models.ForeignKey(to=Item, on_delete=models.CASCADE)