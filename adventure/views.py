from django.shortcuts import render, get_object_or_404


# Create your views here.
from adventure import models

def start(request, name='start'):
    station = get_object_or_404(models.Station, pk=1)
    return render(request, "adventure/station.html", {"station": station})

def station(request, name="station"):
    '''
    Hier wird die passende Station geholt und ausgegeben.
    Wird kein Name übergeben, dann wird die Station 'start' geholt.
    '''
    station = get_object_or_404(models.Station, name=name)
    return render(request, "adventure/station.html", {"station": station})