import requests
import json

from django.conf import settings

def fetch_books(title = None, author = None, isbn = None , publisher = None, subject = None, download = False):
    '''

    fetches

    :param: title
    :param: author
    :param: isbn
    :param: subject
    '''
    parameters = ''
    if title is not None:
        parameters = '+intitle:' +title
    if author is not None:
        parameters += '+inauthor:' + author
    if publisher is not None:
        parameters += '+inpublisher:' + publisher
    if isbn is not None:
        parameters += '+isbn:' + isbn
    if subject is not None:
        parameters += '+subject:' + subject

    query = 'https://www.googleapis.com/books/v1/volumes?q='+parameters
    if download is True:
        query += '&filter=free-ebooks'
    query += '&key='+ settings.GOOGLE_BOOKS_KEY
    #print(query)

    response = requests.get(query)

    if response.status_code == 200:
        book = json.loads(str(json.dumps(response.json(), sort_keys=True, indent=4)))

        print(book)
        authors = book['items'][0]['volumeInfo']['authors']
        title = book['items'][0]['volumeInfo']['title']
        isbn = book['items'][1]['volumeInfo']['industryIdentifiers'][0]['identifier']
        publishedDate = book['items'][0]['volumeInfo']['publishedDate']
        publisher = book['items'][0]['volumeInfo']['publisher']
        description = book['items'][0]['volumeInfo']['description']
        image_url = book['items'][0]['volumeInfo']['imageLinks']['thumbnail']


        return authors, title, isbn, publishedDate, publisher, description, image_url

#print(fetch_books(title='Wardrobe', author='Lewis'))
