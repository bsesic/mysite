from django.db import models

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Language(models.Model):
    language_name = models.CharField(max_length=255)

    def __str__(self):
        return self.language_name


class Book(models.Model):
    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255, blank=True, null=True)
    author  = models.CharField(max_length=255)
    publisher  = models.CharField(max_length=255, blank=True, null=True)
    place  = models.CharField(max_length=255, blank=True, null=True)
    edition  = models.CharField(max_length=255, blank=True, null=True)
    volume  = models.CharField(max_length=255, blank=True, null=True)
    language  = models.CharField(max_length=255, blank=True, null=True)
    price  = models.FloatField(max_length=255, blank=True, null=True)
    isbn  = models.CharField(max_length=255, blank=True, null=True)
    isbn10  = models.CharField(max_length=255, blank=True, null=True)
    publication_date  = models.DateField(blank=True, null=True)
    date_added = models.DateField(auto_now=True)
    image = models.ImageField(upload_to='', blank=True, null=True)
    call_number = models.CharField(max_length=255, blank=True, null=True)
    category = models.ForeignKey(to=Category, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return f"{self.title}: {self.author}"

class Shelf(models.Model):
    name = models.CharField(max_length=255)
    book = models.ForeignKey(to=Book, on_delete=models.PROTECT, blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

class Collection(models.Model):
    name = models.CharField(max_length=255)
    book = models.ForeignKey(to=Book, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.name
