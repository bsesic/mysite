from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponseRedirect

# Create your views here.
from .models import Book, Collection, Shelf, Category

def index(request):
    books = Book.objects.all()
    categories = Category.objects.all()
    collections = Collection.objects.all()
    shelfs = Shelf.objects.all()

    return render(request, 'bookshelf/index.html', {'books': books, 'categories': categories, 'collections': collections, 'shelfs': shelfs } )

def book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'bookshelf/book.html', {'book' : book})