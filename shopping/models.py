from django.db import models

# Create your models here.

class Store(models.Model):
    name = models.CharField

    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length=255)
    amount = models.CharField(max_length=255, null=True)
    unit = models.CharField(max_length=255, null=True)
    store = models.ForeignKey(to=Store, on_delete=models.DO_NOTHING, null=True)
    price = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name

