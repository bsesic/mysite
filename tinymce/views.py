from django.shortcuts import render

# Create your views here.
from .models import Note

def index(request):
    all_notes = Note.objects.all()
    context = { 'result': '',
                'notes': all_notes}

    if request.method == 'POST':
        result = request.POST.get('mytextarea')
        print(result)
        context['result'] = result
        note = Note(note=result)
        note.save()
    return render(request, 'tinymce/index.html', context)
