from django.db import models

# Create your models here.

class Item(models.Model):
    id = models.Index
    name = models.TextField('Name',max_length=100 )
    created_at = models.DateTimeField('date created', auto_now_add=True)
    due_date = models.DateTimeField('due date', blank=True, null=True)
    description = models.TextField('Description', max_length=255, blank=True, null=True)
    done = models.BooleanField(default=False)
    #parent = models.ForeignKey(self, on_delete=models.CASCADE)

    class Meta:
        ordering = ['name', '-name', 'due_date', '-due_date', 'created_at', 'created_at']

    def __str__(self):
        return self.name