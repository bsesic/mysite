from django.urls import path

from . import views

app_name = 'todo'
urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create, name='create'),
    path('update/<int:item_id>/', views.update, name='update'),
    path('delete/<int:item_id>/', views.delete, name='delete'),
    path('done/<int:item_id>/', views.done, name='done'),
]
