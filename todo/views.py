from django.http import HttpResponse, HttpRequest
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.dateparse import parse_datetime

from .models import Item

# Create your views here.

def index(request):

    if request.method == 'POST':
        print(request.POST.get('done'))
        if request.POST.get('done') == "True":
            done = True
        else:
            done = False

        id = request.POST.get('id')
        name=request.POST.get('name')
        due_date = parse_datetime(request.POST.get('due_date'))
        description = request.POST.get('description')
        item = Item(name=name,
                    due_date=due_date,
                    description=description,
                    done=False)

        if id == None:
            #new
            item.save()
        else:
            #update
            Item.objects.filter(id = id).update(name=name,
                    due_date=due_date,
                    description=description,
                    done=done)

    render_items = {'items': Item.objects.all()}

    if request.GET :

        if request.GET['orderby'] == "nameasc":
            render_items = {'items': Item.objects.all().order_by('name')}

        elif request.GET['orderby'] == "namedesc":
            render_items = {'items': Item.objects.all().order_by('-name')}

        if request.GET['orderby'] == "dueasc":
            render_items = {'items': Item.objects.all().order_by('due_date')}

        elif request.GET['orderby'] == "duedesc":
            render_items = {'items': Item.objects.all().order_by('-due_date')}


    return render(request, 'todo/index.html', render_items)

def create(request):
    return render(request, 'todo/create.html', {'task': 'Create'})

def update(request, item_id):
    item = get_object_or_404(Item, pk = item_id)
    return render(request, 'todo/create.html', {'task': 'Update', 'item': item})

def done(request, item_id):
    item = get_object_or_404(Item, pk = item_id)
    if item.done == True:
        item.done = False
    else:
        item.done = True
    item.save()
    return redirect('todo:index')


def delete(request, item_id):
    #item = get_object_or_404(Item, item_id)
    item = Item.objects.get(id = item_id)
    item.delete()
    #delete the item from the database
    return redirect('todo:index')
