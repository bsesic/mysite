from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

from .models import Task

# Create your views here.

def showlist(request):
    taskliste = Task.objects.all()
    print('Anzahl der Tasks:', len(taskliste))
    context = { 'title' : 'Todoliste',
                'taskliste': taskliste
                }
    return render(request, 'todolist/showlist.html', context)

def toggledone(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    task.done = not task.done
    task.save()
    return redirect('showlist')