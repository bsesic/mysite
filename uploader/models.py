from django.db import models
from django.conf import settings

# Create your models here.

class Image(models.Model):
    created_at = models.DateTimeField('date created', auto_now_add=True)
    image_file = models.ImageField(upload_to=settings.MEDIA_ROOT[1:], blank=False) #