from django.shortcuts import render

# Create your views here.
from .models import Image
from .forms import UploadFileForm

def index(request):
    all_images = Image.objects.all()

    context = {'images': all_images}

    if request.method == 'POST':
        '''
        form = UploadFileForm(request.POST, request.FILES)
        
        if form.is_valid:
            print(request.FILES)
            image = Image(image_file=request.FILES['file'])
            image.save()
        '''

        print('Image:', request.FILES['image'])
        image = Image(image_file=request.FILES['image'])
        image.save()

    return render(request, 'uploader/index.html', context=context)